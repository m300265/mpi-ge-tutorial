# MPI-GE tutorial

A tutorial for analysing the MPI-GE (Max Planck Institute Grand Ensemble, http://mpimet.mpg.de/en/grand-ensemble). The tutorial is using python notebooks on the mistral supercomputer.

# Contact
- Yohei Takano (yohei.takano@mpimet.mpg.de)
- Sebastian Milinski (sebastian.milinski@mpimet.mpg.de)

# Setup

To begin, set up a work environment on mistral with jupyterlab and some python packages that will be used in the tutorial. This is described in the [environment setup](environment_setup/README.md)

# Notebooks

Will be added in the near future.