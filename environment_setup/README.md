# Set up local scripts and the environment on mistral

This manual explains how to configure the local system and mistral to start a jupyterlab server on mistral, create a tunnel to the local browser and open the jupyterlab in the browser — just by typing a single command!
(based on a script [provided by DKRZ](https://www.dkrz.de/up/systems/mistral/programming/jupyter-notebook))

Preparing the system involves the following steps:
- create a conda environment on mistral with all required python modules
- create a preload file in the mistral home directory to automatically load the right modules and conda environment when starting the remote connection
- create a local script to automatically start the connection
- (set up ssh keys for login without the need to type your password every time - optional)

## Conda environment on mistral

To use jupyterlab, you need to install a few packages first. We will use [Anaconda](https://www.anaconda.com) to manage the packages. Anacona is already installed on Mistral and allows to have multiple environments with different installed software packages. This comes in handy when different, incompatible software packages are needed for different projects on your computer.

On mistral, load anaconda: `module load anaconda3/bleeding_edge`

Anaconda environments are by default kept in your home directory. The home directory on mistral is limited to 24GB. Every conda environment may use around 4-5GB. Therefore it is recommended to change the default location of conda environments to a different location, for example a project directory in */work*.

To change the default environment directory, edit the *~/.condarc* file.
```
envs_dirs:
  - /work/your_project/m123456/conda-envs
```

### create a new environment
Now you can create a new environment and install python:

`conda create --name jupyterlab_env python=3`

You just created a new environment. Activate the *jupyterlab_env* environment to install software into it:

`source activate jupyterlab_env`

You can list the installed packages with

`conda list` (the list only contains some core packages of python at this point)

Now let's add jupyterlab and a few useful python packages:

`conda install -c conda-forge jupyterlab matplotlib cartopy cdo netcdf4 numpy pandas python-cdo scipy seaborn tqdm xarray`


More conda commands can be found in the [conda cheat sheet](https://docs.conda.io/projects/conda/en/4.6.0/_downloads/52a95608c49671267e40c689e0bc00ca/conda-cheatsheet.pdf)

## preload file on mistral

Now you need to create a small file in your mistral home directory so that the conda environment you created will automatically be used when you start jupyterlab remotely.

In you home directory, create a file new file named *jupyter_preload* with the following content:

```
module unload cdo
module unload netcdf_c
module load anaconda3/bleeding_edge
source activate jupyterlab_env
```

Make sure to adjust the environment name if you modified this in the previous step.

## local shell script
Now copy the [start-jupyter.sh](start-jupyter.sh) script to your local machine. For example to your home directory.

You need to change a few lines in this file:
- set SJ_ACCTCODE to the number of one of your computing projects
- set SJ_USERNAME to your m-number
- change SJ_INCFILE="jupyter_preload" if you modified the name of the *jupyter_preload* file in the previous step.

Save the file and make it executable:

`chmod +x start-jupyter.sh`

Run the script:

`./start-jupyter.sh`

The script will now establish a connection to mistral, start jupyterlab and open your local browser. Leave the terminal window open in the background as long as you are using jupyterlab.

When you are done with your work, save your notebook file, close your browser and stop the script in the terminal using *ctrl + c*


## BONUS: setting up ssh keys
SSH keys can be used to log in to mistral without having to type a password every time. This works for any ssh connection, but also for the [start-jupyter.sh](start-jupyter.sh) script. This is also explained in the [MPI wiki](https://wiki.mpimet.mpg.de/doku.php?id=local_it:use_software:unix_linux:faq).

1. **On your local computer, navigate to the .ssh directory:**

`cd ~/.ssh`

If the directory doesn't exist, create it:

`mkdir ~/.ssh`

2. **create ssh keys**

Run the following command:
`ssh-keygen -t rsa -b 4096`

Provide a name for the ssh keys. (e.g. *work_key*)

Two files will be created in the *.ssh* directory:
*work_key* and *work_key.pub*

The public key can be added to any remote machine to enable login without password. Never share the private key with anyone!

3. **install key on mistral**

Copy the contents of the *work_key.pub* file.

`cat work_key.pub` will print out the contents.

Next, connect to mistral:

`ssh m123456@mistral.dkrz.de`

Go to the .ssh directory:

`cd ~/.ssh`

Add your public key as a new line to the *authorized_keys* file using your favorite text editor.

You might have to create the *authorized_keys* file if it doesn't exist.

To test the connection, log out `exit` and try to connect again `ssh m123456@mistral.dkrz.de`. If the ssh keys are working, you should be logged in without having to type a password.

## BONUS: create an alias for the start-jupyter.sh script
You don't want to navigate to your home directory every time before you start the script? Then you should create an alias.
An alias is an abbrevation for a long command in the command line.

Let's assume you put the *start-jupyter.sh* script in your local home directory ( *~/start-jupyter.sh* )

Add the following line to your *~/.bashrc* file:

`alias misjup='bash ~/start-jupyter.sh -d'`

You can then run the *start-jupyter.sh* script by typing `misjup` in the terminal.

## BONUS: install jupyterlab extensions

There are a number of extensions that add new capabilites to jupyterlab.

To install [jupyterlab-toc](https://github.com/jupyterlab/jupyterlab-toc), a useful extension that adds a table of contents for faster navigation in notebooks, first log in to mistral and activate your conda environment:

```
ssh m12345@mistral.dkrz.de
source activate jupyterlab_env
jupyter labextension install @jupyterlab/toc
```

You might have to close jupyterlab and restart it to see the new extension.

Here is a list of potentially useful extensions. Installation procedure differes and is explained on the github pages. Some extensions require installation of additional packages.

- https://github.com/jupyterlab/jupyterlab-toc
- https://github.com/jupyterlab/jupyterlab-celltags
- https://github.com/dask/dask-labextension
- https://github.com/mflevine/jupyterlab_html
- https://github.com/jupyterlab/jupyterlab-statusbar
- https://github.com/lckr/jupyterlab-variableInspector
- https://github.com/ryantam626/jupyterlab_sublime

Show installed extensions
`jupyter labextension list`



